#!/bin/bash

echo "*** Setting up"
filebeat setup --pipelines --modules mysql,nginx,postgresql,system
filebeat modules enable mysql nginx postgresql system
echo "*** Starting"
exec filebeat "$@"
